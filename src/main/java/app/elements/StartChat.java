package app.elements;

import app.AbstractComponent;
import app.pages.IStaticPage;
import app.settings.Settings;
import org.openqa.selenium.By;

public class StartChat extends AbstractComponent implements IStaticPage {

    private By welcomeHeaderMessage = By.xpath("html/body/table/tbody/tr[1]/td/table/tbody/tr[4]/td[2]/div");

    @Override
    public void open() {
        webDriver.get(Settings.START_CHAT_URL);
    }

    public boolean isStartChatConnect() {
        waitForElementVisible(welcomeHeaderMessage);
        return $(welcomeHeaderMessage).isDisplayed();
    }
}
