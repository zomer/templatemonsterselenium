package app.elements;

import app.AbstractComponent;
import app.pages.FirstCheckoutPage;
import org.openqa.selenium.By;

public class PreviewCheckoutWindow extends AbstractComponent {

    private By checkoutButton = By.xpath(".//*[@id='cart-summary-checkout']");

    public void startCheckout() {
        $E(checkoutButton).click();
    }
}
