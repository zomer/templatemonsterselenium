package app.elements;

import app.AbstractComponent;
import org.openqa.selenium.By;

public class HomeStartChat extends AbstractComponent {

    public void fillStartChat(String email, String fullName) {
        setStartChatFullEmail(email);
        setStartChatFullName(fullName);
    }

    //TODO delete Thread
    public StartChat submitStartChat() throws InterruptedException {
        $(By.xpath(".//*[@id='live-chat-consultant-form-submit']")).submit();

        Thread.sleep(9000);
        return new StartChat();
    }

    private void setStartChatFullEmail(String email) {
        $(By.xpath(".//*[@id='live-chat-consultant-form-email']")).sendKeys(email);
    }

    private void setStartChatFullName(String fullName) {
        $(By.xpath(".//*[@id='live-chat-consultant-form-fullname']")).sendKeys(fullName);
    }
}
