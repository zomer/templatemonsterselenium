package app.pages;

import app.AbstractComponent;
import app.settings.Settings;
import org.openqa.selenium.By;

public class FirstDownloadPage extends AbstractComponent implements IStaticPage {

    @Override
    public void open() {
        webDriver.get(Settings.FIRST_CHECKOUT_FREE_TEMPLATE_PAGE_URL);
    }

    public void selectNewCustomerRadio() {
        $(By.xpath(".//*[@id='new-customer-radio']")).click();
    }

    public SecondDownloadPage clickCheckoutContinueButton() {
        $(By.xpath(".//*[@id='checkout-cont-guest']")).click();
        return new SecondDownloadPage();
    }
}
