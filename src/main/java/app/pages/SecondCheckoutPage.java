package app.pages;

import app.AbstractComponent;
import app.settings.Settings;
import org.openqa.selenium.By;

public class SecondCheckoutPage extends AbstractComponent implements IStaticPage {

    By transactProBuyButton = By.xpath(".//*[@id='checkout-payment-buy-TransactPro']");
    By payPalBuyButton = By.xpath(".//*[@id='checkout-payment-buy-PayPal']");

    @Override
    public void open() {
        webDriver.get(Settings.SECOND_CHECKOUT_PAGE_URL);
    }

    public void payPallPay() {
        selectPayPalTab();
        clickPayPalBuyButton();
    }

    public void transactProPay() {
        selectTransactProTab();
        clickTransactProButton();
    }

    public String getPaymentInfoCustomerName() {
        return $(By.xpath(".//*[@id='payment-info-block']/div[1]/div[3]/div/div[1]/div[1]/div[2]")).getText();
    }

    //TODO Delete Thread
    public static void waitPaymentPage() {
        AbstractComponent.wait(10000);
    }

    private void selectTransactProTab() {
        $(By.xpath(".//*[@id='checkout-payment-TransactPro']")).click();
    }

    private void clickTransactProButton() {
        $V(transactProBuyButton).click();
    }

    private void selectPayPalTab() {
        $(By.xpath(".//*[@id='checkout-payment-PayPal']")).click();
    }

    private void clickPayPalBuyButton() {
        $V(payPalBuyButton).click();
    }
}
