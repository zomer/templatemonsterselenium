package app.pages;

import app.AbstractComponent;

public interface IStaticPage {
    void open();
}
