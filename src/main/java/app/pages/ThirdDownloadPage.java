package app.pages;

import app.AbstractComponent;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.ArrayList;
import java.util.List;

public class ThirdDownloadPage extends AbstractComponent {

    @FindBy(css = ".step-passed")
    private List<WebElement> steps = new ArrayList<>();

    public boolean isBreadcrumbsMarked() {
        waitForElementVisible(By.cssSelector(".step-passed"));
        for (WebElement element : steps) {
            if (!isContainsPassedBreadcrumbsTitle(element.getText())) {
                return false;
            }
        }

        return true;
    }

    public boolean isStatusContentContainsEmail(String userEmail) {
        return $V(By.xpath(".//*[@id='content']/div/div/div/div[2]/div/div[1]/div[1]/p")).getText().contains(userEmail);
    }

    private boolean isContainsPassedBreadcrumbsTitle(String text) {
        if (text.contains("Signin")
                || text.contains("User Details")
                || text.contains("Confirm")) {
            return true;
        } else {
            return false;
        }
    }
}

