package app.pages;

public interface ITemplatePage {
    void open(String url);
}
