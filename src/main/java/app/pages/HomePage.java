package app.pages;

import app.AbstractComponent;
import app.elements.HomeStartChat;
import app.settings.Settings;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends MainTemplatePage implements IStaticPage {

    private By startChat = By.cssSelector(".livechat-girl img");

    public void open() {
        webDriver.get(Settings.HOME_PAGE_URL);
    }

    public HomeStartChat openStartChat() {
        $V(startChat).click();
        return new HomeStartChat();
    }
}
