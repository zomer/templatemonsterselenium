package app.pages;

import app.settings.Settings;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.ArrayList;
import java.util.List;

public class CardPage extends MainTemplatePage implements IStaticPage {

    @FindBy(css = ".js-cart-count")
    private WebElement cartCounter;

    @FindBy(xpath = ".//*[@id='content']/div/div[2]/div[2]/div/div[3]/div/div[1]")
    private WebElement orderTotal;

    @FindBy(xpath = ".//*[@id='content']/div/div[2]/div[2]/div/ul[2]/li[1]/span")
    private WebElement subTotal;

    @FindBy(css = ".js-cart-summary-content")
    private WebElement cartBlock;

    public Integer getSubTotal() {
        return Integer.valueOf(orderTotal.getAttribute("data-price"));
    }

    public Integer getOrderTotal() {
        return Integer.valueOf(orderTotal.getAttribute("data-price"));
    }

    @Override
    public void open() {
        webDriver.get(Settings.CART_CHECKOUT_URL);
    }

    public void cleanCard() {
        removeTemplates();
    }

    public int getProductsCount() {
        return Integer.valueOf(cartCounter.getText());
    }

    //Todo remove Thread
    public void deleteTemplates(List<String> ids) {
        for (WebElement removeIcon: getCardItems()) {
            if (ids.contains(removeIcon.getAttribute("data-product"))) {
                removeIcon.click();
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public List<String> getProductsIds() {
        List ids = new ArrayList();

        for (WebElement removeIcon: getCardItems()) {
            ids.add(removeIcon.getAttribute("data-product"));
        }
        return ids;
    }

    //Todo remove Thread
    private void removeTemplates() {
        for (WebElement removeIcon: getCardItems()) {
            removeIcon.click();
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private List<WebElement> getCardItems() {
        return cartBlock.findElements(By.xpath(".//*[contains(@id, 'sc-dell-template-')]"));
    }
}
