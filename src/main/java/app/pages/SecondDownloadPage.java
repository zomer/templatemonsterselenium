package app.pages;

import app.AbstractComponent;
import app.settings.Settings;
import org.openqa.selenium.By;

public class SecondDownloadPage extends AbstractComponent implements IStaticPage {

    @Override
    public void open() {
        webDriver.get(Settings.SECOND_CHECKOUT_FREE_TEMPLATE_PAGE_URL);
    }

    public void fillUserForm(String fullName, String email, String phone) {
        setUserFormFullName(fullName);
        setUserFormEmail(email);
        setUserFormPhone(phone);
    }

    private void setUserFormFullName(String fullName) {
        $(By.xpath(".//*[@id='freeinfo-form-name']")).sendKeys(fullName);
    }

    private void setUserFormEmail(String email) {
        $(By.xpath(".//*[@id='freeinfo-form-email']")).sendKeys(email);
    }

    private void setUserFormPhone(String phone) {
        $(By.xpath(".//*[@id='freeinfo-form-contactphone']")).sendKeys(phone);
    }

    public ThirdDownloadPage submitUserForm() {
        $(By.xpath(".//*[@id='freeinfo-form-submit']")).submit();
        return new ThirdDownloadPage();
    }
}
