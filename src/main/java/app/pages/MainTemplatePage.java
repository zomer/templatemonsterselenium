package app.pages;

import app.AbstractComponent;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import java.util.concurrent.TimeUnit;

public class MainTemplatePage extends AbstractComponent {

    @FindBy(xpath = ".//*[@id='js-searchForm']/div/span[1]/input[2]")
    private WebElement searchInput;

    @FindBy(xpath = ".//*[@id='searchFormSubmit']")
    private WebElement searchFormSubmitButton;

    @FindBy(xpath = ".//*[@id='menu-account-block']/span")
    private WebElement accoutBlock;

    @FindBy(xpath = ".//*[@id='cart-count']")
    private WebElement cartCount;

    @FindBy(xpath = ".//*[@id='shp-cart']")
    private WebElement cart;

    public CardPage openCard() {
        cart.click();
        return new CardPage();
    }

    public void search() {
        searchFormSubmitButton.click();
    }

    public String getSearchFormSubmitButtonValue() {
        return searchFormSubmitButton.getText();
    }

    public boolean isSearchEmpty() {
        return getSearchFormSubmitButtonValue().isEmpty();
    }

    public void setSearchValue(String searchTemplateId) {
        this.searchInput.sendKeys(searchTemplateId);
    }

    public int getCartCount() {
        return !cartCount.getText().isEmpty() ? Integer.valueOf(cartCount.getText()) : 0;
    }

    public void logout() {
        if (isAccountBlockVisible()) {
            clickAccountBlock();
            clickSignOutTab();
        }
    }

    public SignInPage openSignInPage() {
        clickSignInBlock();
        //Thread.sleep(9000);
        return new SignInPage();
    }

    public void clickSignInBlock() {
        $V(By.xpath(".//*[@id='header-signin-link']")).click();
    }

    public boolean isAccountBlockVisible() {
        return accoutBlock.isDisplayed();
    }

    private void clickAccountBlock() {
        accoutBlock.click();
    }

    private void clickSignOutTab() {
        $(By.xpath(".//*[@id='header-signOut-link']")).click();
    }
}
