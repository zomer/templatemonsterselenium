package app.pages;

import app.AbstractComponent;
import app.settings.Settings;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;

public class SignInPage extends AbstractComponent implements IStaticPage {

    @Override
    public void open() {
        webDriver.get(Settings.SIGN_IN_PAGE_URL);
    }

    public SignInPage fillSingInForm(String testEmail, String testPassword) {
        waitForElementVisible(By.xpath(".//*[@id='edtLogin']"));
        setEmailValue(testEmail);
        setPasswordValue(testPassword);
        return this;
    }

    public HomePage submitSignInForm() {
        new Actions(webDriver).moveToElement($(By.xpath(".//*[@id='login_button']/span"))).click().perform();
        return new HomePage();
    }

    private void setEmailValue(String email) {
        $(By.xpath(".//*[@id='edtLogin']")).sendKeys(email);
    }

    private void setPasswordValue(String password) {
        $V(By.xpath(".//*[@id='edtPassword']")).sendKeys(password);
    }
}
