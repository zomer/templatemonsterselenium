package app.pages;

import app.AbstractComponent;
import app.settings.Settings;
import app.utils.GeneralUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;

import java.util.concurrent.TimeUnit;

public class FirstCheckoutPage extends AbstractComponent implements IStaticPage {

    private By customerEmail = By.xpath(".//*[@id='email-form-email']");

    private By newCustomerFullname = By.xpath(".//*[@id='billinginfo2-form-fullname']");

    private By existCustomerPassword = By.xpath(".//*[@id='signin2-form-password']");

    private By firstCheckoutProductItem = By.xpath(".//*[@id='content']/div/div/div/div[2]/div[2]/div/ul[1]/li");

    @Override
    public void open() {
        webDriver.get(Settings.FIRST_CHECKOUT_PAGE_URL);
    }

    public void fillBillingDetailForm(String fullName, String phone, String city, String postCode, String country) {
        setBillingDetailFormFullName(fullName);
        setBillingDetailFormCityName(city);
        setBillingDetailFormPostalCode(postCode);
        setBillingDetailFormCounty(country);
        setBillingDetailFormPhone(country, phone);
    }

    public String getProductId() {
        return $(firstCheckoutProductItem).getAttribute("data-product");
    }

    private void submitSignInForm() {
        $(By.xpath(".//*[@id='signin2-form-submit']")).click();
    }

    private void fillPasswordExistCustomer(String password) {
        $E(existCustomerPassword).sendKeys(password);
    }

    private void clickSwitchAccount() {
        $(By.xpath(".//*[@id='email-form']/fieldset/legend/span")).click();
    }

    public void selectCustomerTab() {
        $(By.xpath(".//*[@id='customer-form-selectors']/li[2]/a")).click();
    }

    private boolean emaiDisable() {
        return !$(customerEmail).isEnabled();
    }

    private void fillEmailExistCustomer(String email) {
        $E(customerEmail).sendKeys(email);
    }

    public void addNewCustomerEmail(String email) {
        fillEmailForm(email);
        clickCustomerEmail();
    }

    public SecondCheckoutPage submitBillingDetailForm() {
        $(By.xpath(".//*[@id='billinginfo2-form-submit']")).submit();
        return new SecondCheckoutPage();
    }

    private void setBillingDetailFormFullName(String fullName) {
        $E(newCustomerFullname).sendKeys(fullName);
    }

    private void setBillingDetailFormPhone(String country, String phone) {
        clickPhoneCodeSelect();
        fillPhoneSelect(country);
        selectFirstPhone();
        setBillingDetailFormPhoneInput(phone);
    }

    private void setBillingDetailFormPostalCode(String postalCode) {
        $E(By.xpath(".//*[@id='billinginfo2-form-postalcode']")).sendKeys(postalCode);
    }

    private void setBillingDetailFormPhoneInput(String phone) {
        $E(By.xpath(".//*[@id='billinginfo2-form-phone']")).sendKeys(phone);
    }

    private void setBillingDetailFormCityName(String city) {
        $E(By.xpath(".//*[@id='billinginfo2-form-cityname']")).sendKeys(city);
    }

    private void setBillingDetailFormCounty(String country) {
        clickCountrySelect();
        fillCountrySelect(country);
        selectFirstCountry();
    }

    public void selectNewCustomerTab() {
        $(By.xpath(".//*[@id='customer-form-selectors']/li[1]/a")).click();
    }

    public SecondCheckoutPage authorize(String email, String password) {
        fillEmailExistCustomer(email);
        clickCustomerPassword();
        fillPasswordExistCustomer(password);
        submitSignInForm();
        return new SecondCheckoutPage();
    }

    public void switchAccount() {
        if (emaiDisable()) {
            clickSwitchAccount();
        }
    }

    private void clickCountrySelect() {
        $(By.xpath(".//*[@id='billinginfo2_form_countryiso2_chosen']/a")).click();
    }

    private void fillCountrySelect(String country) {
        $(By.xpath(" .//*[@id='billinginfo2_form_countryiso2_chosen']/div/div/input")).sendKeys(country);
    }

    private void selectFirstCountry() {
        $(By.xpath(".//*[@id='billinginfo2_form_countryiso2_chosen']/div/ul/li")).click();
    }

    private void clickPhoneCodeSelect() {
        $(By.xpath(".//*[@id='billinginfo2_form_phone_code_chosen']/a/i")).click();
    }

    private void fillPhoneSelect(String country) {
        $E(By.xpath(".//*[@id='billinginfo2_form_phone_code_chosen']/div/div/input")).sendKeys(country);
    }

    private void selectFirstPhone() {
        $(By.xpath(".//*[@id='billinginfo2_form_phone_code_chosen']/div/ul/li")).click();
    }

    private void fillEmailForm(String email) {
        $(customerEmail).sendKeys(email);
    }

    private void clickCustomerPassword() {
        new Actions(webDriver).moveToElement($E(existCustomerPassword)).click().perform();
    }

    private void clickCustomerEmail() {
        new Actions(webDriver).moveToElement($E(customerEmail)).click().perform();
    }

    public boolean isInvalidMessageShow(int checkCount) throws Exception {
        for (int i = 0; i < checkCount; i++) {
            addNewCustomerEmail(i + "zzzzzz@gmail");

            //TODO Delete Thread
            Thread.sleep(2000);

            if (!getInvalidMessage().equals(Settings.INVALID_EMAIL_MESSAGE)) {
                return false;
            }

            cleanCustomerEmail();
        }

        return true;
    }

    private String getInvalidMessage() {
        return $V(By.xpath("//*[@id=\"email-form\"]/fieldset/div/div/div[3]/div[2]")).getText();
    }

    private void cleanCustomerEmail() {
        $(customerEmail).clear();
    }
}
