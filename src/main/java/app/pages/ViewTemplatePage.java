package app.pages;

import app.AbstractComponent;
import org.openqa.selenium.By;

public class ViewTemplatePage extends AbstractComponent {
    public FirstDownloadPage downloadProduct() {
        $(By.xpath(".//*[contains(@id, 'livedemo-buy-now-free-')] ")).click();
        return new FirstDownloadPage();
    }
}
