package app.pages;

import app.AbstractComponent;
import app.elements.PreviewCheckoutWindow;
import app.settings.Settings;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.ArrayList;
import java.util.List;

public class TemplatePage extends MainTemplatePage implements ITemplatePage {

    @FindBy(xpath = ".//*[@id='searchFormSubmit']")
    private WebElement searchFormSubmitButton;

    @FindBy(css = ".key-feature")
    List<WebElement> viewImageBlock = new ArrayList<>();

    @FindBy(xpath = ".//*[@id='demo_bottom']")
    private WebElement demoViewButton;

    public ViewTemplatePage openViewTemplate() {
        demoViewButton.click();
        return new ViewTemplatePage();
    }

    public void open(String url) {
        webDriver.get(url);
    }

    public void closeSpam() {
        WebElement webElement = $(By.id("gcd-close"));
        if (webElement.isDisplayed()) {
            webElement.click();
        }
    }

    public PreviewCheckoutWindow addToCart() {
        $(By.xpath(".//*[contains(@id, 'preview-add-to-cart-regular-')]")).click();
        return new PreviewCheckoutWindow();
    }

    public void buy() {
        addToCart().startCheckout();
    }

    public void scrollOverViewImages() throws Exception {
        for (WebElement element: viewImageBlock) {
            if((Boolean) ((JavascriptExecutor)webDriver).executeScript("return arguments[0].complete && typeof arguments[0].naturalWidth != \"undefined\" && arguments[0].naturalWidth > 0", element.findElement(By.tagName("img")))) {
                ((JavascriptExecutor)webDriver).executeScript("arguments[0].scrollIntoView();", element);
            } else {
                throw new Exception("scrollOverViewImages issue");
            }
        }
    }

}
