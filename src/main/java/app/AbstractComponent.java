package app;

import app.settings.Settings;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOfElementLocated;

public abstract class AbstractComponent {
    public static WebDriver webDriver;
    private String parentWindowHandle;
    private String localWindowHandle;

    public AbstractComponent() {
        waitAbstractPage(webDriver).until(initHandler());
        PageFactory.initElements(webDriver, this);
    }

    public WebElement $(By locator) {
        return assertThat(visibilityOfElementLocated((locator)));
    }

    public WebElement $E(By locator) {
        waitForElementEnable(locator);
        return assertThat(visibilityOfElementLocated((locator)));
    }

    public WebElement $V(By locator) {
        waitForElementVisible(locator);
        return assertThat(visibilityOfElementLocated((locator)));
    }

    public List<WebElement> $Collection(By locator) {
        waitForElementVisible(locator);
        return assertThat(ExpectedConditions.presenceOfAllElementsLocatedBy(locator));
    }

    public static void quit() {
        webDriver.quit();
    }


    public void waitForElementEnable(final By by) {
        assertThat(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                WebElement element = webDriver.findElement(by);
                return element != null && element.isEnabled();
            }
        });
    }

    public void waitForElementVisible(final By by) {
        assertThat(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                WebElement element = webDriver.findElement(by);
                return element != null && element.isDisplayed();
            }
        });
    }

    public static void wait(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void srollToBotton() {
        JavascriptExecutor jse = (JavascriptExecutor)webDriver;
        jse.executeScript("window.scrollTo(0,Math.max(document.documentElement.scrollHeight,document.body.scrollHeight,document.documentElement.clientHeight));");
    }

    public  void swithToParentHandler() {
        webDriver.switchTo().window(parentWindowHandle);
    }

    public static void refreshPage() {
        webDriver.navigate().refresh();
    }

    private void assertThat(Predicate condition) {
        (new WebDriverWait(webDriver, Settings.WEB_DRIVER_WAIT_SECOND)).until(condition);
    }

    private  <V> V assertThat(Function<? super WebDriver, V> condition) {
        return (new WebDriverWait(webDriver, Settings.WEB_DRIVER_WAIT_SECOND)).until(condition);
    }

    private  ExpectedCondition<?> initHandler() {
        return new ExpectedCondition<String>() {
            public String apply(WebDriver driver) {
                parentWindowHandle = webDriver.getWindowHandle();
                List<String> handlesList = new ArrayList<>(webDriver.getWindowHandles());
                localWindowHandle = handlesList.get(handlesList.size() - 1);
                webDriver.switchTo().window(localWindowHandle);
                return localWindowHandle;
            }
        };
    }

    private static WebDriverWait waitAbstractPage(WebDriver driver) {
        return new WebDriverWait(driver, 40, 1000);
    }
}
