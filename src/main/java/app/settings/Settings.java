package app.settings;

public  interface Settings {
    String HOME_PAGE_URL = "http://www.templatemonster.com";
    String SIGN_IN_PAGE_URL = "https://wac.templatemonster.com/signin.html";
    String TEMPLATE_58935_URL = "http://www.templatemonster.com/magento-themes/glassini-glasses-store-magento-theme-58935.html";
    String FIRST_CHECKOUT_FREE_TEMPLATE_PAGE_URL = "https://secure.templatemonster.com/checkout/step1.html";
    String FIRST_CHECKOUT_PAGE_URL = "https://secure.templatemonster.com/checkout/";
    String SECOND_CHECKOUT_PAGE_URL = "https://secure.templatemonster.com/checkout/payment/";
    String START_CHAT_URL = "http://chat.template-help.com/chat.jsp";
    String CART_CHECKOUT_URL = "https://secure.templatemonster.com/cart.php";
    String FREE_TEMPLATE_PAGE_URL = "http://www.templatemonster.com/free-templates/free-html5-theme-sport-site.php";
    String SECOND_CHECKOUT_FREE_TEMPLATE_PAGE_URL = "https://secure.templatemonster.com/checkout/step2.html";
    String TEMPLATE_52112_URL = "http://www.templatemonster.com/wordpress-themes/52112.html";
    String TEMPLATE_60100_URL = "http://www.templatemonster.com/shopify-themes/belly-dance-shopify-theme-60100.html";

    String INVALID_EMAIL_MESSAGE = "Please specify a valid email";

    String PAY_PAL_URL = "www.paypal.com";
    String TRANSACT_PRO_URL = "https://www2.1stpayments.net";

    String TEST_LOGIN_EMAIL = "zomer.pro@mail.ru";
    String TEST_PASSWORD = "Zambezi0989805";
    String TEST_FULL_NAME = "Misha Marhain";

    int WEB_DRIVER_WAIT_SECOND = 60;
    int PAGE_LOAD_SECOND = 60;

    String HOME_PAGE_TITLE = "Website Templates | Web Templates | Template Monster";
    String COOKIE_WAC_VALUE = "1";
    String SEARCH_TEMPLATE_ID = "58905";
    String SEARCH_FREE_TEMPLATE_ID = "51682";

    String NEW_CUSTOMER_FULL_NAME = "TEST TEST";
    String NEW_CUSTOMER_PHONE = "0931870075";
    String NEW_CUSTOMER_CITY = "NIKOLAEV";
    String NEW_CUSTOMER_POST_CODE = "0512";
    String NEW_CUSTOMER_COUNTRY = "ukraine";
    String DELETE_TEMPLATE_ID = "58935";
    int DELETE_TEMPLATE_SUM = 179;
}
