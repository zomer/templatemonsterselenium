package app.utils;

import app.pages.CardPage;

import java.util.ArrayList;
import java.util.List;

public class OrderHelper {

    int subOrderPrice = 0;
    int orderPrice = 0;
    List<String> products = new ArrayList<>();

    public void updateManage(CardPage cardPage) {
        this.subOrderPrice = this.subOrderPrice + cardPage.getSubTotal();
        this.orderPrice = this.orderPrice + cardPage.getOrderTotal();
        this.products.addAll(cardPage.getProductsIds());
    }

    public int quantity() {
        return products.size();
    }

    public boolean isIdentical(List cardProducts) {
        return products.containsAll(cardProducts);
    }

    public int getSubOrderPrice() {
        return subOrderPrice;
    }

    public int getOrderPrice() {
        return orderPrice;
    }
}
