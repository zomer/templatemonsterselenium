import app.AbstractComponent;
import app.elements.HomeStartChat;
import app.elements.StartChat;
import app.pages.*;
import app.settings.Settings;
import app.utils.GeneralUtils;
import app.utils.OrderHelper;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

public class TemplatemonsterSiteTests extends BaseTest {

    @Test
    public void testSignForm() {
        HomePage homePage = new HomePage();
        homePage.open();

        SignInPage signInPage = homePage.openSignInPage();
        signInPage.open();
        signInPage.fillSingInForm(Settings.TEST_LOGIN_EMAIL, Settings.TEST_PASSWORD)
                .submitSignInForm();

        assertEquals(Settings.HOME_PAGE_TITLE, webDriver.getTitle());
        assertEquals(Settings.COOKIE_WAC_VALUE, webDriver.manage().getCookieNamed("wac").getValue());
    }

    @Test
    public void testTemplateSearch() {
        TemplatePage templatePage = searchTemplate(Settings.SEARCH_TEMPLATE_ID);
        assertTrue(templatePage.isSearchEmpty());
        assertTrue(webDriver.getTitle().contains(Settings.SEARCH_TEMPLATE_ID));
    }

    @Test
    public void testPayPalPayment() {
        HomePage homePage = new HomePage();

        homePage.open();

        buyTemplate(Settings.TEMPLATE_58935_URL);

        FirstCheckoutPage firstCheckoutPage = new FirstCheckoutPage();

        firstCheckoutPage.selectNewCustomerTab();
        firstCheckoutPage.addNewCustomerEmail(GeneralUtils.generateNewCustomerEmail());
        firstCheckoutPage.fillBillingDetailForm(
                Settings.NEW_CUSTOMER_FULL_NAME,
                Settings.NEW_CUSTOMER_PHONE,
                Settings.NEW_CUSTOMER_CITY,
                Settings.NEW_CUSTOMER_POST_CODE,
                Settings.NEW_CUSTOMER_COUNTRY);

        SecondCheckoutPage secondCheckoutPage = firstCheckoutPage.submitBillingDetailForm();

        assertEquals(Settings.NEW_CUSTOMER_FULL_NAME, secondCheckoutPage.getPaymentInfoCustomerName());

        secondCheckoutPage.payPallPay();

        SecondCheckoutPage.waitPaymentPage();
        assertTrue(webDriver.getCurrentUrl().contains(Settings.PAY_PAL_URL));
    }

    @Test
    public void testTransactProPayment() {
        HomePage homePage = new HomePage();

        homePage.open();

        buyTemplate(Settings.TEMPLATE_58935_URL);

        SecondCheckoutPage secondCheckoutPage = autorizeInCheckout();

        assertEquals(Settings.TEST_FULL_NAME, secondCheckoutPage.getPaymentInfoCustomerName());
        secondCheckoutPage.transactProPay();

        SecondCheckoutPage.waitPaymentPage();
        assertTrue(webDriver.getCurrentUrl().contains(Settings.TRANSACT_PRO_URL));
    }

    @Test
    public void testFeeTemplatePurchase() {
        CardPage cardPage = new CardPage();
        cardPage.open();
        cardPage.cleanCard();

        TemplatePage templatePage = searchTemplate(Settings.SEARCH_FREE_TEMPLATE_ID);

        ViewTemplatePage viewTemplatePage = templatePage.openViewTemplate();
        FirstDownloadPage downloadPage = viewTemplatePage.downloadProduct();
        downloadPage.selectNewCustomerRadio();

        SecondDownloadPage secondDownloadPage = downloadPage.clickCheckoutContinueButton();

        String userEmail = GeneralUtils.generateNewCustomerEmail();

        secondDownloadPage.fillUserForm(Settings.NEW_CUSTOMER_FULL_NAME, userEmail, Settings.NEW_CUSTOMER_PHONE);
        ThirdDownloadPage thirdDownloadPage = secondDownloadPage.submitUserForm();

        assertTrue(thirdDownloadPage.isStatusContentContainsEmail(userEmail));
        assertTrue(thirdDownloadPage.isBreadcrumbsMarked());

        HomePage homePage = new HomePage();
        homePage.open();

        assertTrue(homePage.isAccountBlockVisible());
    }

    @Test
    public void testCartMerge() {
        OrderHelper orderHelper = new OrderHelper();

        CardPage cardPage = addProductsForTestUserHelper(orderHelper);

        cardPage.logout();
        cardPage.cleanCard();

        buyTemplate(Settings.TEMPLATE_60100_URL);

        cardPage.open();

        orderHelper.updateManage(cardPage);

        HomePage homePage = cardPage.openSignInPage()
                .fillSingInForm(Settings.TEST_LOGIN_EMAIL, Settings.TEST_PASSWORD)
                .submitSignInForm();

        assertEquals(homePage.getCartCount(), orderHelper.quantity());

        cardPage = homePage.openCard();

        assertTrue(orderHelper.isIdentical(cardPage.getProductsIds()));
        assertEquals(cardPage.getOrderTotal().intValue(), orderHelper.getOrderPrice());
        assertEquals(cardPage.getSubTotal().intValue(), orderHelper.getSubOrderPrice());
    }

    @Test
    public void testChat() throws InterruptedException {
        HomePage homePage = new HomePage();
        homePage.open();

        HomeStartChat homeStartChat = homePage.openStartChat();

        homeStartChat.fillStartChat(Settings.TEST_LOGIN_EMAIL, Settings.TEST_FULL_NAME);

        StartChat startChat = homeStartChat.submitStartChat();

        assertTrue(webDriver.getCurrentUrl().contains(Settings.START_CHAT_URL));
        assertTrue(startChat.isStartChatConnect());
    }

    @Test
    public void testOverviewLazyLoad() throws Exception {
        TemplatePage paidTemplatePage = new TemplatePage();
        paidTemplatePage.open(Settings.TEMPLATE_52112_URL);
        paidTemplatePage.scrollOverViewImages();
        paidTemplatePage.srollToBotton();
    }

    @Test
    public void testNotValidEmailInCheckout() throws Exception {
        buyTemplate(Settings.TEMPLATE_58935_URL);
        FirstCheckoutPage firstCheckoutPage = new FirstCheckoutPage();
        firstCheckoutPage.selectNewCustomerTab();
        assertTrue(firstCheckoutPage.isInvalidMessageShow(10));
    }

    @Test
    public void testRemovingMergeProducts() {
        OrderHelper orderHelper = new OrderHelper();
        addProductsForTestUserHelper(orderHelper);

        HomePage homePage = new HomePage();
        homePage.open();
        homePage.logout();

        CardPage cardPage = homePage.openCard();
        cardPage.cleanCard();

        buyTemplate(Settings.TEMPLATE_58935_URL);
        cardPage.open();
        orderHelper.updateManage(cardPage);
        autorizeInCheckout();

        cardPage.open();
        cardPage.deleteTemplates(removedTemplates());

        AbstractComponent.refreshPage();

        assertTrue(isProductsRemoved(cardPage.getProductsIds()));
        assertEquals(deletedPrice(orderHelper, cardPage), Settings.DELETE_TEMPLATE_SUM);
    }

    private int deletedPrice(OrderHelper orderHelper, CardPage cardPage) {
        return orderHelper.getOrderPrice() - cardPage.getOrderTotal();
    }

    private boolean isProductsRemoved(List products) {
        return !products.containsAll(removedTemplates());
    }

    private List removedTemplates() {
        List removeTemplates = new ArrayList<>();
        removeTemplates.add(Settings.DELETE_TEMPLATE_ID);
        return removeTemplates;
    }

    private SecondCheckoutPage autorizeInCheckout() {
        FirstCheckoutPage firstCheckoutPage = new FirstCheckoutPage();
        firstCheckoutPage.open();
        firstCheckoutPage.selectCustomerTab();
        firstCheckoutPage.switchAccount();

        return firstCheckoutPage.authorize(Settings.TEST_LOGIN_EMAIL, Settings.TEST_PASSWORD);
    }


    private TemplatePage searchTemplate(String id) {
        HomePage homePage = new HomePage();
        homePage.open();

        homePage.setSearchValue(id);
        homePage.search();

        return new TemplatePage();
    }

    public CardPage addProductsForTestUserHelper(OrderHelper orderHelper) {
        CardPage cardPage = addProductsForTestUser();
        orderHelper.updateManage(cardPage);
        return cardPage;
    }

    public CardPage addProductsForTestUser() {

        SignInPage signInPage = new SignInPage();
        signInPage.open();
        signInPage.fillSingInForm(Settings.TEST_LOGIN_EMAIL, Settings.TEST_PASSWORD);

        HomePage homePage = signInPage.submitSignInForm();
        CardPage cardPage = homePage.openCard();
        cardPage.cleanCard();
        buyTemplate(Settings.TEMPLATE_52112_URL);

        cardPage.open();

        return cardPage;
    }

    public void buyTemplate(String url) {
        TemplatePage templatePage = new TemplatePage();
        templatePage.open(url);
        templatePage.buy();
    }
}
