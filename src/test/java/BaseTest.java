import app.AbstractComponent;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class BaseTest {

    protected static WebDriver webDriver;

    @Before
    public void setUp() {
        webDriver = new FirefoxDriver();
        webDriver.manage().window().maximize();
        AbstractComponent.webDriver = webDriver;
    }

    @After
    public void teaDown() {
        AbstractComponent.quit();
    }
}


